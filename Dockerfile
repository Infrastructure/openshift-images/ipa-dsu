FROM registry.access.redhat.com/ubi9/python-311:1

ADD ipa-ca.crt /etc/ipa-ca.crt

ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD ipa-dsu.py /usr/local/bin/ipa-dsu.py
CMD ["python", "/usr/local/bin/ipa-dsu.py"]
