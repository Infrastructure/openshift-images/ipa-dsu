import os
import sys
from datetime import datetime, timedelta

import gitlab
import ldap
import pytz
from dateutil import parser

LDAP_USER = os.getenv("LDAP_USER")
LDAP_PASSWORD = os.getenv("LDAP_PASSWORD")
LDAP_HOST = os.getenv("LDAP_HOST")
LDAP_USER_BASE = os.getenv("LDAP_USER_BASE")
LDAP_GROUP_BASE = os.getenv("LDAP_GROUP_BASE")
LDAP_CA_PATH = os.getenv("LDAP_CA_PATH")
GITLAB_PRIVATE_RW_TOKEN = os.getenv("GITLAB_PRIVATE_RW_TOKEN")
GITLAB_URL = "https://gitlab.gnome.org"
GITLAB_PROVIDER = "ldapmain"

EXCLUDED_USERNAMES = [
    "csoriano-gitlab-admin-account",
    "damned-lies-gitlab-sa",
    "flathub_aarch64",
    "git",
    "gitlab-accounts-automation-sa",
    "gitlab-bot",
    "gitlab-bugzilla-migration",
    "gitmaster",
    "gnome-build-meta-bot",
    "gnome_aarch64",
    "gnomecvs",
    "gnomeftp",
    "gnomesdk",
    "gnomeweb",
    "hookshot-bot-element-sa",
    "openshift-gitlab-automation",
    "splunk-service-account",
    "sysadmin-scripts-ro-01",
    "sysadmin-scripts-ro-02",
    "sysadmin-scripts-rw-02",
    "sysadmin-scripts-rw-03",
]

EXCLUDED_GROUPS = ["staffmail", "foundation", "gnomecvs", "gnomecvs_exceptions"]


def create_ldap_connection():
    if LDAP_CA_PATH:
        ldap.set_option(ldap.OPT_X_TLS_CACERTFILE, LDAP_CA_PATH)

    try:
        ldap_conn = ldap.ldapobject.ReconnectLDAPObject(f"ldaps://{LDAP_HOST}:636")
        ldap_conn.protocol_version = ldap.VERSION3
        ldap_conn.set_option(ldap.OPT_REFERRALS, 0)

        ldap_conn.simple_bind_s(LDAP_USER, LDAP_PASSWORD)
        print("LDAP connection successful")
        return ldap_conn
    except ldap.SERVER_DOWN:
        print(f"Unable to connect to LDAP server at ldaps://{LDAP_HOST}:636")
    except ldap.INVALID_CREDENTIALS:
        print("Invalid LDAP credentials")
    except ldap.LDAPError as e:
        print(f"LDAP error: {str(e)}")
    return None


def safe_decode(value):
    if isinstance(value, bytes):
        try:
            return value.decode("utf-8")
        except UnicodeDecodeError:
            return f"Unable to decode: {value}"
    return value


def get_group_members(ldap_conn, group_name):
    members = set()
    search_filter = f"(&(objectClass=groupOfNames)(cn={group_name}))"
    attrs = ["member"]
    try:
        result = ldap_conn.search_s(
            LDAP_GROUP_BASE, ldap.SCOPE_SUBTREE, search_filter, attrs
        )
        for dn, entry in result:
            if "member" in entry:
                members.update(
                    safe_decode(member).split(",")[0].split("=")[1]
                    for member in entry["member"]
                )
    except ldap.LDAPError as e:
        print(f"LDAP error when fetching members of {group_name}: {str(e)}")
    return members


def get_inactive_users(ldap_conn, excluded_members):
    excluded_uids = "".join(f"(!(uid={username}))" for username in EXCLUDED_USERNAMES)
    search_filter = (
        f"(&(objectClass=person)(!(sn=Service Account))(!(uid=*-sa)){excluded_uids}"
        f"(memberOf=cn=ipausers,{LDAP_GROUP_BASE}))"
    )
    attrs = ["uid", "krbLastSuccessfulAuth"]

    try:
        result = ldap_conn.search_s(
            LDAP_USER_BASE, ldap.SCOPE_SUBTREE, search_filter, attrs
        )

        inactive_users = []
        two_years_ago = datetime.now(pytz.UTC) - timedelta(days=730)

        for dn, entry in result:
            uid = safe_decode(entry["uid"][0]) if "uid" in entry else "Unknown"

            if uid in excluded_members:
                continue

            if "krbLastSuccessfulAuth" not in entry:
                inactive_users.append((uid, "Never logged in"))
            else:
                auth_time = safe_decode(entry["krbLastSuccessfulAuth"][0])
                try:
                    auth_datetime = datetime.strptime(auth_time, "%Y%m%d%H%M%SZ")
                    auth_datetime = pytz.UTC.localize(auth_datetime)
                    if auth_datetime < two_years_ago:
                        inactive_users.append((uid, auth_datetime.strftime("%Y-%m-%d")))
                except ValueError:
                    inactive_users.append((uid, f"Unable to parse: {auth_time}"))

        return inactive_users
    except ldap.LDAPError as e:
        print(f"LDAP error: {str(e)}")
        return []


def get_gitlab_user_info(gl, uid):
    try:
        users = gl.users.list(
            extern_uid=f"uid={uid},{LDAP_USER_BASE}", provider=GITLAB_PROVIDER
        )
        if users:
            return users[0]
    except gitlab.exceptions.GitlabListError as e:
        if "not found" not in str(e).lower():
            print(f"GitLab error for {uid}: {str(e)}")
    except gitlab.exceptions.GitlabError as e:
        print(f"GitLab error for {uid}: {str(e)}")
    return None


def parse_date(date_str):
    if date_str:
        dt = parser.parse(date_str)
        if dt.tzinfo is None:
            dt = dt.replace(tzinfo=pytz.UTC)
        return dt
    return None


def is_active_on_gitlab(user, two_years_ago):
    last_sign_in = parse_date(user.last_sign_in_at)
    current_sign_in = parse_date(user.current_sign_in_at)
    last_activity = parse_date(user.last_activity_on)

    return any(
        activity and activity > two_years_ago
        for activity in (last_sign_in, current_sign_in, last_activity)
    )


def remove_from_group(ldap_conn, username, group_name, dry_run=False):
    if dry_run:
        print(f"[DRY RUN] Would remove {username} from {group_name} group")
        return True

    try:
        group_dn = f"cn={group_name},{LDAP_GROUP_BASE}"
        mod_attrs = [
            (
                ldap.MOD_DELETE,
                "member",
                f"uid={username},{LDAP_USER_BASE}".encode("utf-8"),
            )
        ]
        ldap_conn.modify_s(group_dn, mod_attrs)
        return True
    except ldap.LDAPError as e:
        print(f"Error removing {username} from {group_name} group: {str(e)}")
        return False


def disable_inactive_user(ldap_conn, username, dry_run=False):
    if dry_run:
        return True

    user_dn = f"uid={username},{LDAP_USER_BASE}"
    ipausers_dn = f"cn=ipausers,{LDAP_GROUP_BASE}"

    try:
        user_entry = ldap_conn.search_s(
            user_dn, ldap.SCOPE_BASE, "(objectclass=*)", ["nsAccountLock"]
        )
        is_locked = (
            user_entry[0][1].get("nsAccountLock", [b"FALSE"])[0].lower() == b"true"
        )

        ipausers_entry = ldap_conn.search_s(
            ipausers_dn, ldap.SCOPE_BASE, "(objectclass=*)", ["member"]
        )
        is_in_ipausers = user_dn.encode("utf-8") in ipausers_entry[0][1].get(
            "member", []
        )

        modifications = []

        if not is_locked:
            modifications.append((ldap.MOD_REPLACE, "nsAccountLock", [b"TRUE"]))

        if is_in_ipausers:
            modifications.append((ldap.MOD_DELETE, "member", [user_dn.encode("utf-8")]))

        if not modifications:
            return True

        if modifications:
            for mod in modifications:
                if mod[1] == "nsAccountLock":
                    ldap_conn.modify_s(user_dn, [mod])
                    print(f"Locked account for {username}")
                elif mod[1] == "member":
                    ldap_conn.modify_s(ipausers_dn, [mod])
                    print(f"Removed {username} from ipausers group")

        return True

    except ldap.NO_SUCH_ATTRIBUTE:
        return True
    except ldap.LDAPError as e:
        print(f"Error disabling user {username}: {str(e)}")
        return False


def main(dry_run):
    ldap_conn = None
    try:
        ldap_conn = create_ldap_connection()
        if not ldap_conn:
            print("Failed to establish LDAP connection. Exiting.")
            return

        gl = gitlab.Gitlab(GITLAB_URL, private_token=GITLAB_PRIVATE_RW_TOKEN)
        gl.auth()

        two_years_ago = datetime.now(pytz.UTC) - timedelta(days=730)

        print("Inactive gnomecvs members (No GitLab activity for 2 years):")
        print("=" * 100)
        print(
            f"{'FreeIPA UID':<20} | {'GitLab Username':<20} | {'Last GitLab Activity':<20} | {'Action Taken'}"
        )
        print("-" * 100)

        gnomecvs_members = get_group_members(ldap_conn, "gnomecvs")
        gnomecvs_actions_taken = 0
        for uid in gnomecvs_members:
            gitlab_user = get_gitlab_user_info(gl, uid)
            if gitlab_user:
                activities = [
                    parse_date(gitlab_user.last_sign_in_at),
                    parse_date(gitlab_user.current_sign_in_at),
                    parse_date(gitlab_user.last_activity_on),
                ]
                valid_activities = [a for a in activities if a]
                if valid_activities:
                    last_activity = max(valid_activities)
                    if last_activity < two_years_ago:
                        action_taken = (
                            "Would be removed from gnomecvs"
                            if dry_run
                            else "Removed from gnomecvs"
                            if remove_from_group(ldap_conn, uid, "gnomecvs", dry_run)
                            else "Failed to remove from gnomecvs"
                        )
                        print(
                            f"{uid:<20} | {gitlab_user.username:<20} | {last_activity.strftime('%Y-%m-%d'):<20} | {action_taken}"
                        )
                        gnomecvs_actions_taken += 1

        print("=" * 100)
        print(
            f"gnomecvs group processing completed. Actions taken: {gnomecvs_actions_taken}"
        )
        print()

        print("Inactive ftpadmin members (No GitLab activity for 2 years):")
        print("=" * 100)
        print(
            f"{'FreeIPA UID':<20} | {'GitLab Username':<20} | {'Last GitLab Activity':<20} | {'Action Taken'}"
        )
        print("-" * 100)

        ftpadmin_members = get_group_members(ldap_conn, "ftpadmin")
        ftpadmin_actions_taken = 0
        for uid in ftpadmin_members:
            gitlab_user = get_gitlab_user_info(gl, uid)
            if gitlab_user:
                activities = [
                    parse_date(gitlab_user.last_sign_in_at),
                    parse_date(gitlab_user.current_sign_in_at),
                    parse_date(gitlab_user.last_activity_on),
                ]
                valid_activities = [a for a in activities if a]
                if valid_activities:
                    last_activity = max(valid_activities)
                    if last_activity < two_years_ago:
                        action_taken = (
                            "Would be removed from ftpadmin"
                            if dry_run
                            else "Removed from ftpadmin"
                            if remove_from_group(ldap_conn, uid, "ftpadmin", dry_run)
                            else "Failed to remove from ftpadmin"
                        )
                        print(
                            f"{uid:<20} | {gitlab_user.username:<20} | {last_activity.strftime('%Y-%m-%d'):<20} | {action_taken}"
                        )
                        ftpadmin_actions_taken += 1

        print("=" * 100)
        print(
            f"ftpadmin group processing completed. Actions taken: {ftpadmin_actions_taken}"
        )
        print()

        excluded_members = set()
        for group in EXCLUDED_GROUPS:
            excluded_members.update(get_group_members(ldap_conn, group))

        inactive_users = get_inactive_users(ldap_conn, excluded_members)

        print("Inactive users (no activity in FreeIPA or GitLab for 2 years):")
        print("=" * 100)
        print(
            f"{'FreeIPA UID':<20} | {'GitLab Username':<20} | {'Last FreeIPA Auth':<20} | {'Last GitLab Activity':<20} | {'Action Taken'}"
        )
        print("-" * 100)

        truly_inactive_users = []

        for uid, last_auth in sorted(inactive_users):
            try:
                gitlab_user = get_gitlab_user_info(gl, uid)
                if gitlab_user and is_active_on_gitlab(gitlab_user, two_years_ago):
                    continue

                gitlab_username = gitlab_user.username if gitlab_user else "Not found"
                gitlab_last_activity = "N/A"
                if gitlab_user:
                    activities = [
                        parse_date(gitlab_user.last_sign_in_at),
                        parse_date(gitlab_user.current_sign_in_at),
                        parse_date(gitlab_user.last_activity_on),
                    ]
                    valid_activities = [a for a in activities if a]
                    if valid_activities:
                        gitlab_last_activity = max(valid_activities).strftime(
                            "%Y-%m-%d"
                        )

                if dry_run:
                    action_taken = "Account would be disabled"
                else:
                    action_taken = (
                        "Account disabled"
                        if disable_inactive_user(ldap_conn, uid, dry_run)
                        else "Failed to disable"
                    )

                print(
                    f"{uid:<20} | {gitlab_username:<20} | {last_auth:<20} | {gitlab_last_activity:<20} | {action_taken}"
                )
                truly_inactive_users.append(uid)
            except Exception as e:
                print(f"Error processing user {uid}: {str(e)}")

        print("=" * 100)
        print(f"Total inactive users processed: {len(truly_inactive_users)}")

        if dry_run:
            print(
                "\nThis was a dry run. No actual changes were made to the FreeIPA system."
            )

    except ldap.LDAPError as e:
        print(f"LDAP error: {str(e)}")
    except gitlab.exceptions.GitlabAuthenticationError:
        print("GitLab authentication failed. Check your GITLAB_PRIVATE_RW_TOKEN.")
    except Exception as e:
        print(f"An error occurred: {str(e)}")
    finally:
        if ldap_conn:
            try:
                ldap_conn.unbind_s()
            except:
                pass


if __name__ == "__main__":
    dry_run = "--dry-run" in sys.argv

    required_vars = [
        "LDAP_USER",
        "LDAP_PASSWORD",
        "LDAP_HOST",
        "LDAP_USER_BASE",
        "LDAP_GROUP_BASE",
        "GITLAB_PRIVATE_RW_TOKEN",
    ]
    missing_vars = [var for var in required_vars if not os.getenv(var)]

    if missing_vars:
        print(
            f"Error: The following required environment variables are not set: {', '.join(missing_vars)}"
        )
        sys.exit(1)
    else:
        main(dry_run)
